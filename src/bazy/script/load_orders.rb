#!/usr/bin/env ruby
#coding: utf-8

require 'nokogiri'
require 'active_support/core_ext/hash/conversions'
#require 'active_support/dependencies'
require 'active_support/all'
require 'active_record'

RAILS_ROOT = File.dirname(__FILE__) + "/.."
#RAILS_ROOT = "/home/liadon/agh/bazy/src/bazy/"
LIB_DIR = "#{RAILS_ROOT}/lib"
MODELS_DIR = "#{RAILS_ROOT}/app/models"
DB_CONFIG = "#{RAILS_ROOT}/config/database.yml"

require 'active_support/dependencies'
ActiveSupport::Dependencies.autoload_paths += Dir["#{LIB_DIR}/**/"]
ActiveSupport::Dependencies.autoload_paths += Dir["#{MODELS_DIR}/"]
::ActiveRecord::Base.establish_connection(
    YAML.load(File.read(DB_CONFIG))["development"])

data_files = {
	"orders_rand_10000.xml" => "order",
	"orders_rand_20000.xml" => "order",
	"orderdetails_rand_10000.xml" => "orderdetail",
	"orderdetails_rand_20000.xml" => "orderdetail",
}

OrderDetail.destroy_all
Order.destroy_all

orders = {}

data_files.each do |file_name, entity|
	puts "loading "+file_name
	file = File.open("/media/ae684228-b317-46e1-855b-8bcbb4007f3b/Study/6sem/Bazy/Projekt/bazy/src/bazy/data/"+file_name)
	doc = Nokogiri::XML(file)
	records = doc.xpath("//"+entity)
#	records = records[1..2000]

	records.each do |record|
		h = Hash.from_xml(record.to_s)[entity]
		class_name = entity.capitalize
		class_name = "OrderDetail" if class_name == "Orderdetail"
		entity_class = Object.const_get(class_name)
		obj = entity_class.new(h)
		obj.id = h[entity_class.primary_key] #if class_name=="Order"

		if class_name == "Order"
			orders[h['OrderID']] = { :order => obj, :details => [] }
		else
			if orders[h['OrderID']]!=nil
				orders[h['OrderID']][ :details ].push(obj)
			end
		end
	end
	file.close
end

start = Time.now
orders.each_with_index do |entry, index|
	order = entry.second
	#puts order
	#puts order[:details]
	#puts order[:order]
	order[:order].save!
	order[:details].each do |detail|
		detail.save!
	end
	if (index+1) % 1000 == 0
		now = Time.now
		puts now-start
		start = now
	end
end
