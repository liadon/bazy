#!/usr/bin/env ruby
#coding: utf-8

require 'nokogiri'
require 'active_support/core_ext/hash/conversions'
require 'active_support/dependencies'
require 'active_support/all'
require 'active_record'

RAILS_ROOT = File.dirname(__FILE__) + "/.."
#RAILS_ROOT = "/home/liadon/agh/bazy/src/bazy/"
LIB_DIR = "#{RAILS_ROOT}/lib"
MODELS_DIR = "#{RAILS_ROOT}/app/models"

DB_CONFIG = "#{RAILS_ROOT}/config/database.yml"

require 'active_support/dependencies'
ActiveSupport::Dependencies.autoload_paths += Dir["#{LIB_DIR}/**/"]
ActiveSupport::Dependencies.autoload_paths += Dir["#{MODELS_DIR}/"]
::ActiveRecord::Base.establish_connection(
    YAML.load(File.read(DB_CONFIG))["development"])

data_files = {
	"categories.xml" => "category",
	"customers.xml" => "customer",
	"employees.xml" => "employee",
	"products.xml" => "product",
	"shippers.xml" => "shipper",
	"suppliers.xml" => "supplier"
}

Category.destroy_all
Customer.destroy_all
Employee.destroy_all
Product.destroy_all
Shipper.destroy_all
Supplier.destroy_all

data_files.each do |file_name, entity|
	file = File.open("../data/"+file_name)
	doc = Nokogiri::XML(file)
	records = doc.xpath("//"+entity)

	records.each do |record|
		h = Hash.from_xml(record.to_s)[entity]
		class_name = entity.capitalize
		class_name = "OrderDetail" if class_name == "Orderdetail"
		entity_class = Object.const_get(class_name)
		obj = entity_class.new(h)
#		puts record.to_s, h, obj
		obj.id = h[entity_class.primary_key]
		obj.save!
#		obj.reload.id
	end

	file.close
end
