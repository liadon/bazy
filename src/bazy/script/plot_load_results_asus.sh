#!/usr/bin/env gnuplot

set terminal pngcairo
set output "out.png"
set title "Porównanie czasów ładowania danych (asus)"
set ylabel "średni czas zapisu zamówienia [m]"
set yrange [0:80]
plot "results/asus_slow" title "przed tuningiem", "results/asus_fast" title "po tuningu"
