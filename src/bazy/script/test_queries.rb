#!/usr/bin/env ruby
#coding: utf-8

require 'nokogiri'
require 'active_support/core_ext/hash/conversions'
require 'active_support/dependencies'
require 'active_support/all'
require 'active_record'

RAILS_ROOT = File.dirname(__FILE__) + "/.."
#RAILS_ROOT = "/home/liadon/agh/bazy/src/bazy/"
LIB_DIR = "#{RAILS_ROOT}/lib"
MODELS_DIR = "#{RAILS_ROOT}/app/models"
DB_CONFIG = "#{RAILS_ROOT}/config/database.yml"

require 'active_support/dependencies'
ActiveSupport::Dependencies.autoload_paths += Dir["#{LIB_DIR}/**/"]
ActiveSupport::Dependencies.autoload_paths += Dir["#{MODELS_DIR}/"]
::ActiveRecord::Base.establish_connection(
    YAML.load(File.read(DB_CONFIG))["development"])

def test_a
	return Order.joins(:customer).group("\"Country\"").count
end
def test_b
	years = Hash.new
	Order.find_each do |order|
		order_id = order["OrderID"]
		year = order["OrderDate"].year
		time = order["ShippedDate"].jd - order["OrderDate"].jd
		years[year] = [] if not years.key?(year)
		years[year].push(time)
	end
	years.each do |year,list|
		value = list.inject{ |sum, el| sum + Float(el) }.to_f / list.size
		value = Integer(100*value)/100.0
		years[year] = value
	end
	return years
end
def test_c
	quantity_per_product = OrderDetail.group('"ProductID"').sum('"Quantity"')
	quantity_per_supplier = Hash.new(0)
	quantity_per_product.each do |productID, quantity|
		quantity = Integer(quantity)
		supplier = Product.find(productID)["SupplierID"]
		quantity_per_supplier[supplier] += quantity
	end
	return quantity_per_supplier
end
def test_d
	price_per_weekday = Hash.new(0.0)
	OrderDetail.find_each do |detail|
		order = Order.find(detail["OrderID"])
		wday = order["OrderDate"].wday
		unit_price = Float(detail["UnitPrice"])
		quantity = Integer(detail["Quantity"])
		value = unit_price * quantity
		price_per_weekday[wday] += value
	end
	return price_per_weekday
end

def test_e
	sum = {}
	OrderDetail.find_each do |detail|
		order = Order.find(detail["OrderID"])
		year = order["OrderDate"].year
		customer = Customer.find(order["CustomerID"])
		country = customer["Country"]
		price = Integer(detail["Quantity"])*Float(detail["UnitPrice"])
		sum[country] = {} if not sum.key?(country)
		sum[country][year] = 0.0 if not sum[country].key?(year)
		sum[country][year] += price
	end
	sum.each do |country, map|
		map.each do |year, value|
			value = Integer(100*value)/100.0
			sum[country][year] = value
		end
	end
	return sum
end

def test_f
	avg = {}
	OrderDetail.find_each do |detail|
		order = Order.find(detail["OrderID"])
		year = order["ShippedDate"].year
		shipper = order["ShipVia"]
		quantity = Integer(detail["Quantity"])
		avg[shipper] = {} if not avg.key?(shipper)
		avg[shipper][year] = {:quantity => 0, :price => 0.0} if not avg[shipper].key?(year)
		avg[shipper][year][:quantity] += quantity
		avg[shipper][year][:price] += quantity * detail["UnitPrice"];
	end
	avg.each do |shipper, avg_per_shipper|
		avg_per_shipper.each do |year, map|
			value = map[:price]/map[:quantity]
			value = Integer(100*value)/100.0
			avg[shipper][year] = value
		end
	end
	return avg
end

tests = [
	lambda {test_a},
	lambda {test_b},
	lambda {test_c},
	lambda {test_d},
	lambda {test_e},
	lambda {test_f}
]

start = Time.now
tests.each_with_index do |test, index|
	res = test.call()
	now = Time.now
	time = now-start
	puts "#{index})\n#{res}\n[#{time}]"
	start = now
end
