class Employeeterritory < ActiveRecord::Base
  attr_accessible :EmployeeID, :TerritoryID

  validates :EmployeeID, :presence => true
  validates :TerritoryID, :presence => true, :length => {:maximum => 20}

  belongs_to :employee, :class_name => 'Employee', :foreign_key => 'EmployeeID'
  belongs_to :territory, :class_name =>'Territory', :foreign_key => 'TerritoryID'
end
