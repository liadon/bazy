class Shipper < ActiveRecord::Base
  attr_accessible :CompanyName, :Phone
#  set_primary_key :ShipperID
  self.primary_key = 'ShipperID'

  validates :CompanyName, :presence => true, :length => {:maximum => 40}
  validates :Phone, :length => {:maximum => 24}
  
  has_many :orders, :class_name => 'Order', :foreign_key => 'ShipVia', :primary_key => 'ShipperID'
end
