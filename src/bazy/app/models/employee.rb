class Employee < ActiveRecord::Base
  attr_accessible :FirstName, :LastName, :Title, :TitleOfCourtesy, :BirthDate, :HireDate, :Address, :City, :Region, :PostalCode, :Country, :HomePhone, :Extension, :Photo, :Notes, :ReportsTo, :PhotoPath
#  set_primary_key :EmployeeID
  self.primary_key = 'EmployeeID'

 validates :FirstName, :presence => true, :length => {:maximum => 10}
 validates :LastName, :presence => true, :length => {:maximum => 20}
 validates :Title, :length => {:maximum => 30}
 validates :TitleOfCourtesy, :length => {:maximum => 25}
 validates :Address, :length => {:maximum => 60}
 validates :City, :length => {:maximum => 15}
 validates :Region, :length => {:maximum => 15}
 validates :PostalCode, :length => {:maximum => 10}
 validates :Country, :length => {:maximum => 15}
 validates :HomePhone, :length => {:maximum => 24}
 validates :Extension, :length => {:maximum => 4}
 validates :PhotoPath, :length => {:maximum => 255}

 has_many :reporters, :class_name => 'Employee', :foreign_key => 'ReportsTo'
 belongs_to :sheef, :class_name => 'Employee', :foreign_key => 'ReportsTo'

#  has_and_belongs_to_many :territories, :class_name => 'Territory', :join_table => 'employeeterritories', :foreign_key => 'EmployeeID', :association_foreign_key => 'TerritoryID'
end
