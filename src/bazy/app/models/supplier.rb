class Supplier < ActiveRecord::Base
  attr_accessible :CompanyName, :ContactName, :ContactTitle, :Address, :City, :Region, :PostalCode, :Country, :Phone, :Fax, :HomePage

#  set_primary_key :SupplierID
  self.primary_key = 'SupplierID'

  validates :CompanyName, :presence => true, :length => {:maximum => 40}
  validates :ContactName, :length => {:maximum => 30}
  validates :ContactTitle, :length => {:maximum => 30}
  validates :Address, :length => {:maximum => 60} 
  validates :City, :length => {:maximum => 15}
  validates :Region, :length => {:maximum => 15} 
  validates :PostalCode, :length => {:maximum => 10} 
  validates :Country, :length => {:maximum => 15} 
  validates :Phone, :length => {:maximum => 24} 
  validates :Fax, :length => {:maximum => 24} 

  has_many :products, :class_name => 'Product', :foreign_key => 'SupplierID', :primary_key => 'SupplierID'
end
