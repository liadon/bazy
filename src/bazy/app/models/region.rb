class Region < ActiveRecord::Base
  attr_accessible :RegionDescription

  set_table_name "region"
#  set_primary_key :RegionID
  self.primary_key = 'RegionID'

  validates :RegionDescription, :presence => true, :length => {:maximum => 50}

#  has_many :territories, :class_name => 'Territory', :foreign_key => 'RegionID', :primary_key => 'RegionID'
end
