class Customercustomerdemo < ActiveRecord::Base
  attr_accessible :CustomerTypeID, :CustomerID
  set_table_name "customercustomerdemo"

  validates :CustomerTypeID, :presence => true
  validates :CustomerID, :presence => true

  belongs_to :customer, :class_name => 'Customer', :foreign_key => 'CustomerID'
  belongs_to :customerdemographic, :class_name => 'Customerdemographic', :foreign_key => 'CustomerTypeID'
end
