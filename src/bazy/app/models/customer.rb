class Customer < ActiveRecord::Base
  attr_accessible :CustomerID, :CompanyName, :ContactName, :ContactTitle, :Address, :City, :Region, :PostalCode, :Country, :Phone, :Fax
#  set_primary_key :CustomerID
  self.primary_key = 'CustomerID'

  validates :CustomerID, :presence => true, :length => {:maximum => 5}
  validates :CompanyName, :presence => true, :length => {:maximum => 40}
  validates :ContactName, :length => {:maximum => 30}
  validates :ContactTitle, :length => {:maximum => 30}
  validates :Address, :length => {:maximum => 60}
  validates :City, :length => {:maximum => 15}
  validates :Region, :length => {:maximum => 15}
  validates :PostalCode, :length => {:maximum => 10}
  validates :Country, :length => {:maximum => 15}
  validates :Phone, :length => {:maximum => 24}
  validates :Fax, :length => {:maximum => 24}


  has_and_belongs_to_many :customerdemographics, :class_name => 'Customerdemographic', :join_table => 'customercustomerdemo', :association_foreign_key => 'CustomerTypeID', :foreign_key => 'CustomerID' 
end
