class OrderDetail < ActiveRecord::Base
  attr_accessible :OrderID, :ProductID, :UnitPrice, :Quantity, :Discount
  self.primary_key = 'odID'
   
  validates :OrderID, :presence => true 
  validates :ProductID, :presence => true 
  validates :UnitPrice, :presence => true 
  validates :Quantity, :presence => true 
  validates :Discount, :presence => true 

  belongs_to :order, :class_name => 'Order', :foreign_key => 'OrderID'
  belongs_to :product, :class_name => 'Product', :foreign_key => 'ProductID'
end
