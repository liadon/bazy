class Customerdemographic < ActiveRecord::Base
  attr_accessible :CustomerTypeID, :CustomerDesc
  set_primary_key :CustomerTypeID

  validates :CustomerTypeID, :presence => true, :length => {:maximum => 10}

  has_and_belongs_to_many :customers, :class_name => 'Customer', :join_table => 'customercustomerdemo', :foreign_key => 'CustomerTypeID', :association_foreign_key => 'CustomerID'
end
