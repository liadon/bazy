class Category < ActiveRecord::Base
   attr_accessible  :CategoryName, :Description, :Picture
#   set_primary_key :CategoryID
   self.primary_key = 'CategoryID'
   
   validates :CategoryName , length: {maximum: 15} , :presence =>  true
   
   has_many :products , :class_name => 'Product', :foreign_key => 'CategoryID'
end
