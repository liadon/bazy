class Product < ActiveRecord::Base
   attr_accessible :ProductName, :SupplierID, :CategoryID, :QuantityPerUnit, :UnitPrice, :UnitsInStock, :UnitsOnOrder, :ReorderLevel, :Discontinued
#   set_primary_key :ProductID
  self.primary_key = 'ProductID'

   validates :ProductName, :presence => true, :length => {:maximum => 40}
   validates :QuantityPerUnit,  :length => {:maximum => 20}
   validates :Discontinued , :presence => true, :inclusion => {:in =>0..1, :message => "value can be eather 0 or 1. You have used %{value}"}
   

   belongs_to :supplier , :class_name => 'Supplier', :foreign_key => 'SupplierID'
   belongs_to :category , :class_name => 'Category', :foreign_key => 'CategoryID'
   has_and_belongs_to_many :orders, :class_name => 'Order', :join_table => 'order_details', :foreign_key => 'ProductID', :association_foreign_key => 'OrderID'
end
