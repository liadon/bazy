class Territory < ActiveRecord::Base
  attr_accessible :TerritoryID, :TerritoryDescription, :RegionID
#  set_primary_key :TerritoryID
  self.primary_key = 'TerritoryID'

  validates :TerritoryID, :presence => true, :length => {:maximum => 20}, :uniqueness => true
  validates :TerritoryDescription, :presence => true, :length => {:maximum => 50}
  validates :RegionID, :presence => true

#  has_and_belongs_to_many :employees, :class_name => 'Employee', :join_table => 'employeeterritories', :foreign_key => 'TerritoryID', :association_foreign_key => 'EmployeeID'

  belongs_to :region , :class_name => 'Region', :foreign_key => 'RegionID'
end
