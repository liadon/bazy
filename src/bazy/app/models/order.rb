class Order < ActiveRecord::Base
  attr_accessible :CustomerID, :EmployeeID, :OrderDate, :RequiredDate, :ShippedDate, :ShipVia, :Freight, :ShipName, :ShipAddress, :ShipCity, :ShipRegion, :ShipPostalCode, :ShipCountry
#  set_primary_key :OrderID 
  self.primary_key = 'OrderID'

  validates :CustomerID, :length => {:maximum => 5} 
  validates :ShipName, :length => {:maximum => 40}
  validates :ShipAddress, :length => {:maximum => 60}
  validates :ShipCity, :length => {:maximum => 15}
  validates :ShipRegion, :length => {:maximum => 15}
  validates :ShipPostalCode, :length => {:maximum => 15}
  validates :ShipCountry, :length => {:maximum => 15}


  belongs_to :customer, :class_name => 'Customer', :foreign_key => 'CustomerID'
  belongs_to :shipper, :class_name => 'Shipper', :foreign_key => 'ShipVia'
  belongs_to :employee, :class_name => 'Employee', :foreign_key => 'EmployeeID'
  has_many :order_details, :class_name => 'OrderDetail', :foreign_key => 'OrderID'
  has_and_belongs_to_many :products, :class_name => 'Product', :join_table => 'order_details', :foreign_key => 'OrderID', :association_foreign_key => 'ProductID'
end
