class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.float :mark

      t.timestamps
    end
  end
end
