class AddReferencesToMarks < ActiveRecord::Migration
  def up
    add_column :marks , :student_id , :integer
    add_column :marks, :subject_id, :integer	
  end

  def down
    remove_column :marks, :student_id
    remove_column :marks, :subject_id
  end
end
