class FaculciesController < ApplicationController
  # GET /faculcies
  # GET /faculcies.json
  def index
    @faculcies = Faculcy.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @faculcies }
    end
  end

  # GET /faculcies/1
  # GET /faculcies/1.json
  def show
    @faculcy = Faculcy.find(params[:id])
    @students = @faculcy.students

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @faculcy }
    end
  end

  # GET /faculcies/new
  # GET /faculcies/new.json
  def new
    @faculcy = Faculcy.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @faculcy }
    end
  end

  # GET /faculcies/1/edit
  def edit
    @faculcy = Faculcy.find(params[:id])
  end

  # POST /faculcies
  # POST /faculcies.json
  def create
    @faculcy = Faculcy.new(params[:faculcy])

    respond_to do |format|
      if @faculcy.save
        format.html { redirect_to @faculcy, notice: 'Faculcy was successfully created.' }
        format.json { render json: @faculcy, status: :created, location: @faculcy }
      else
        format.html { render action: "new" }
        format.json { render json: @faculcy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /faculcies/1
  # PUT /faculcies/1.json
  def update
    @faculcy = Faculcy.find(params[:id])

    respond_to do |format|
      if @faculcy.update_attributes(params[:faculcy])
        format.html { redirect_to @faculcy, notice: 'Faculcy was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @faculcy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /faculcies/1
  # DELETE /faculcies/1.json
  def destroy
    @faculcy = Faculcy.find(params[:id])
    @faculcy.destroy

    respond_to do |format|
      format.html { redirect_to faculcies_url }
      format.json { head :no_content }
    end
  end
end
