class Subject < ActiveRecord::Base
  attr_accessible :title

  validates_presence_of :title

  has_many :marks , :inverse_of => :subject
end
