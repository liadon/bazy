class Mark < ActiveRecord::Base
  attr_accessible :mark
  
  validates_presence_of :mark

  belongs_to :student , :inverse_of => :marks
  belongs_to :subject , :inverse_of => :marks
end
