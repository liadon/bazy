class Student < ActiveRecord::Base
  attr_accessible :name, :surname
  validates_presence_of :name
  validates_presence_of :surname

  has_many :marks , :inverse_of => :student 
  belongs_to :faculcy , :inverse_of => :students
end
