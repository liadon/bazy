class Faculcy < ActiveRecord::Base
  attr_accessible :title

  validates_presence_of :title

  has_many :students , :inverse_of => :faculcy
end
