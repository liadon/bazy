require 'test_helper'

class FaculciesControllerTest < ActionController::TestCase
  setup do
    @faculcy = faculcies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:faculcies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create faculcy" do
    assert_difference('Faculcy.count') do
      post :create, faculcy: { title: @faculcy.title }
    end

    assert_redirected_to faculcy_path(assigns(:faculcy))
  end

  test "should show faculcy" do
    get :show, id: @faculcy
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @faculcy
    assert_response :success
  end

  test "should update faculcy" do
    put :update, id: @faculcy, faculcy: { title: @faculcy.title }
    assert_redirected_to faculcy_path(assigns(:faculcy))
  end

  test "should destroy faculcy" do
    assert_difference('Faculcy.count', -1) do
      delete :destroy, id: @faculcy
    end

    assert_redirected_to faculcies_path
  end
end
